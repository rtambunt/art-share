import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'hgkdqd9e',
    dataset: 'dataset'
  }
})
