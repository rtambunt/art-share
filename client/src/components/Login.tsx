import { GoogleLogin } from "@react-oauth/google";
import jwt_decode from "jwt-decode";
import logo from "../assets/logowhite.png";
import video from "../assets/share.mp4";
import { useNavigate } from "react-router-dom";

import { client } from "../client";
import { Decoded } from "../utils/decoded";

const Login = () => {
  const navigate = useNavigate();

  return (
    <div>
      <div className="flex items-center flex-col h-screen">
        <div className="relative w-full h-full">
          <video
            src={video}
            loop
            controls={false}
            muted
            autoPlay
            className="w-full h-full object-cover"
          />
          <div className="absolute flex flex-col justify-center items-center top-0 left-0 bottom-0 right-0 bg-blackOverlay">
            <div className="p-5">
              <img src={logo} alt="logo" width="130px" />
            </div>

            <div className="shadow-2xl">
              <GoogleLogin
                onSuccess={(credentialResponse) => {
                  const decoded: Decoded = jwt_decode(
                    credentialResponse.credential!
                  );
                  console.log(decoded);

                  localStorage.setItem("user", JSON.stringify(decoded));

                  const doc = {
                    _id: decoded.sub,
                    _type: "user",
                    userName: decoded.name,
                    image: decoded.picture,
                  };

                  console.log(doc);

                  client.createIfNotExists(doc).then(() => {
                    console.log("successful creation!");
                    navigate("/", { replace: true });
                  });
                }}
                onError={() => {
                  console.log("Login Failed");
                }}
              />
            </div>
          </div>
        </div>
        <div>Menu</div>
      </div>
    </div>
  );
};

export default Login;
