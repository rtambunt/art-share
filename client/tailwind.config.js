/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundColor: {
        blackOverlay: "rgba(0, 0, 0, 0.7)",
      },

      keyframes: {
        slidein: {
          "0%": {
            "-webkit-transform": "translateX(-200px)",
            transform: "translateX(-200px)",
          },
          "100%": {
            "-webkit-transform": "translateX(0px)",
            transform: "translateX(0px)",
          },
        },
      },
      animation: {
        "slide-in": "slidein 0.5s ease-out",
      },
    },
  },
  plugins: [],
};
